#!/usr/bin/php
<?PHP

if ($argc > 1)
{
	$i = 0;
	$s = implode(" ", $argv);
	$s = explode(" ", $s);
	$s = array_filter($s, 'strlen');
	unset($s[0]);
	foreach ($s as $elem)
	{
		if (ctype_alpha($elem[0]))
			$alpha[] = $elem;
		else if (is_numeric($elem))
			$num[] = $elem;
		else
			$spec[] = $elem;
	}
	natcasesort($alpha);
	rsort($num, SORT_NUMERIC);
	sort($spec);
	$res = array_merge($alpha, $num, $spec);
	foreach ($res as $elem)
		echo ($elem."\n");
}

?>
