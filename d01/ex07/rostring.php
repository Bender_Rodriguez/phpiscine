#!/usr/bin/php
<?php

function ft_split($str)
{
	$str = trim($str);
	$str = explode(" ", $str);
	$str = array_filter($str);
	return ($str);
}

$res = ft_split($argv[1]);
$i = 1;
while ($res[$i])
{
	print($res[$i]);
	print(" ");
	$i++;
}
print($res[0]);

?>
