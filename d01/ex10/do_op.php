#!/usr/bin/php
<?php
if ($argc == 4)
{
	$res;
	$left = intval($argv[1]);
	$op = trim($argv[2]);
	$right = intval($argv[3]);
	if ($op ==  "+")
		$res = $left + $right;
	else if ($op == "-")
		$res = $left - $right;
	else if ($op == "/")
		$res = $left / $right;
	else if ($op =="%")
		$res = $left % $right;
	else if ($op == "*")
		$res = $left * $right;
	echo $res;
	echo "\n";
}
else
	echo("Incorrect Parameters\n");
?>
