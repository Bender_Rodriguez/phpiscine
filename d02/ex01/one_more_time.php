#!/usr/bin/php
<?php
date_default_timezone_set('Europe/Paris');
if ($argc > 1)
{
	$day = "([Ll]undi|[Mm]ardi|[Mm]ercredi|[Jj]eudi|[Vv]endredi|[Ss]amedi]|[Dd]imanche)";
	$number = "[0-9]{1,2}";
	$month = "([Jj]anvier|[Ff]evrier|[Mm]ars|[Aa]vril|[Mm]ai|[Jj]uin|[Jj]uillet|[Aa]out";
	$month .= "[Ss]eptembre|[Oo]ctobre|[Nn]ovembre|[Dd]ecembre)";
	$year = "[0-9]{4}";
	$time = "[0-9]{2}:[0-9]{2}:[0-9]{2}";
	$date = "/".$day." ".$number." ".$month." ".$year." ".$time."/";

	if (preg_match($date, $argv[1]))
	{
		$res = explode(' ', $argv[1]);
		$number = $res[1];
		if (strlen($number) < 2)
			$number = "0".$number;
		if (preg_match("/[Jj]anvier/", $res[2]))
			$month = "01";
		if (preg_match("/[Ff]evrier/", $res[2]))
			$month = "02";
		if (preg_match("/[Mm]ars/", $res[2]))
			$month = "03";
		if (preg_match("/[Aa]vril/", $res[2]))
			$month = "04";
		if (preg_match("/[Mm]ai/", $res[2]))
			$month = "05";
		if (preg_match("/[Jj]uin/", $res[2]))
			$month = "06";
		if (preg_match("/[Jj]uiller/", $res[2]))
			$month = "07";
		if (preg_match("/[Aa]out/", $res[2]))
			$month = "08";
		if (preg_match("/[Ss]eptembre/", $res[2]))
			$month = "09";
		if (preg_match("/[Oo]ctobre/", $res[2]))
			$month = "10";
		if (preg_match("/[Nn]ovembre/", $res[2]))
			$month = "11";
		if (preg_match("/[Dd]ecembre/", $res[2]))
			$month = "12";
		print(strtotime($res[3]."-".$month."-".$number." ".$res[4])."\n");
	}
	else
		print("Wrong Format\n");
}

?>
