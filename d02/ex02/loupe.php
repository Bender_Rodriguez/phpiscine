#!/usr/bin/php
<?php
function check(array $match)
{
	return ('<a'.$match[1].'>'.strtoupper($match[2]).'>');
}

function title($matches)
{
    return 'title="'.strtoupper($matches[1]).'"';
}

$fd = fopen($argv[1], "r");
$regex = '/<a([^>]+)>([\w\s]+)</';
while ($line = fgets($fd))
{
	$line = preg_replace_callback($regex, "check", $line);
	$line = preg_replace_callback('/title="([\w\s]+)"/', "title", $line);
	print($line);
}
?>
