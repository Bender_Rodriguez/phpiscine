#!/usr/bin/php
<?php

function	curlito($str)
{
	$c = curl_init($str);
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($c);
	curl_close($c);
	return $data;
}

if (count($argv) > 1)
{
	if (preg_match("/http:\/\/.+/", $argv[1]))
		$dir = substr($argv[1], 7);
	else
		$dir = $argv[1];
	$str = curlito($argv[1]);
	if (preg_match_all("/<img.*src=\"([^\"]+)\".*/", $str, $match) != 0)
	{
		mkdir($dir);
		foreach ($match[1] as $elem)
		{
			$img = curlito($elem);
			$tab = explode('/', $elem);
			$fd = fopen($dir."/".end($tab), "w");
			fwrite($fd, $img);
			fclose($fd);
		}
	}
}

?>
